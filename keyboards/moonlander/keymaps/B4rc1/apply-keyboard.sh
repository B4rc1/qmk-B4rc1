#!/bin/sh

set -e

cleanup () {
  rm -fr new_source
}
trap cleanup EXIT

FW_FILE=$(realpath "$1")
cd "/home/jonas/nerdshit/moonlander/qmk_firmware_zsa/keyboards/moonlander/keymaps/B4rc1/"

unzip -j "$FW_FILE" "moonlander_puq-and-chill_source*" -d new_source > /dev/null

# remove trailing whitespaces
sed -i 's/[ \t]*$//' new_source/keymap.c

patch new_source/keymap.c my.patch # > /dev/null


[[ $(diff new_source/keymap.c keymap.c | wc -l) -eq 0 ]] && echo "file already installed" && exit 1

# the '&& true' is needed because otherwise delta does not return
delta keymap.c new_source/keymap.c && true



while true; do
    read -p "Do you wish to override the current layout with this layout? [Y/n] " yn
    case $yn in
        [Yy]* ) echo "patching"; break;;
        [Nn]* ) exit 1;;
        * ) echo "Please answer yes or no [y/n].";;
    esac
done

cp new_source/keymap.c keymap.c
git add keymap.c
git commit

# cleanup

echo "keyboard layout has been patched"
